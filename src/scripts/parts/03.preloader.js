// Loader
(function() {
	setTimeout(function() {
		$('#preloader').fadeOut(600);
		$('body').removeClass('loading');
	}, 3000);
})();
