// 1. Sign up form
(function() {
	var $form = $('#sign-up-form'),
		$submit = $('#sign-up-submit');

	$form.validate({
		errorClass: "error",
		highlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').removeClass(errorClass).addClass(validClass);
		},
		messages: {
			name: "enter your name",
		}
	});

	$form.on('submit', function (e) {
		var formData = new FormData($(this)[0]);

		if ($(this).valid()) {
			$.ajax({
				type: $(this).attr('method'),
				url: $(this).attr('action'),
				processData: false,
				contentType: false,
				data: formData,
				success: function(data)
				{
					$form.append('<p class="form__success-message">This is a success message! Lorem ipsum dolor sit amet</p>')
				}
			});
		}

		e.preventDefault();
	});

	$form.find(':input[required]').on('change', function () {
		var valid = $form.valid();
		$submit.prop('disabled', ! valid);
	});

})();


