(function() {
	var $burger = $('#menu-burger'),
		$mobileMenu = $('#mobile-menu'),
		$header = $('#header');

	$burger.click(function() {
		$mobileMenu.fadeToggle(300);
		$('body').toggleClass('menu-open');
		$header.toggleClass('header_menu-open');
		// $burger.toggleClass('header__burger_menu-open');
		$burger.toggleClass('header__burger_open');
		return false;
	});

})();