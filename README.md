### To see project in browser ###
- Choose build branch - it's important.

- Open index.html from build  folder.

- Sources are in src folder.

### How to use ###
- Clone this repo:

		git clone git@bitbucket.org:eva_filippova/parrot-wings-test.git
	
	
	
- And then in command line type  (to install all dependencies):

		npm install 
	
	
- or 

		yarn
	
	

- Run local server(http://localhost:1423) and let magic happen

		gulp

### Чтобы посмотреть верстку в браузере ###
- Чтобы посмотреть верстку в браузере, необходимо выбрать ветку build(ВАЖНО!)

- Затем открыть index.html из папки build.

- Исходники можно посмотреть в папке src

### Как развернуть проект у себя ###

- Склонировать репозиторий:

		git clone git@bitbucket.org:eva_filippova/parrot-wings-test.git
	
	
	
- И написать в коммандной строке (для утсановки зависимостей):

		npm install
	
	

- или

		yarn
	
	
	
- Запустить локальный сервер(http://localhost:1423)

		gulp



